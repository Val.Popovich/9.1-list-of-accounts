import DateTime from './DateTime.js';

class DateFormatter {

  static formatDateTime(date=DateTime.getCurTime(), format='YYYY-MM-DD hh:mm:ss') {
    return moment(date).format(format)
  }

  static formatDate(date=DateTime.getCurTime(), format='YYYY-MM-DD') {
    return moment(date).format(format)
  }
}

export default DateFormatter;