import Task from './Task.js';

class Note extends Task {
  constructor(data) {
    super(data);
    this.type = 'note';
  }
}

export default Note;