import DateTime from './DateTime.js';

class Task {
    constructor(data) {
      this.id = data.id;
      this.text = data.text;
      this.reminderTime = data.reminderTime;
      this.overdueTime = data.overdueTime;
      this.creationDate = data.creationDate;
      this.isOverdue = DateTime.isOverdueTime(this.overdueTime);
      this.isNotificationNeeded = true;
  }
}

export default Task;