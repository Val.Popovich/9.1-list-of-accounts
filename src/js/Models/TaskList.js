import TaskFactory from './TaskFactory.js';
import DateTime from './DateTime.js';
import Todo from './Todo.js';
import TaskSubscriber from './TaskSubscriber.js';
import TaskListInitialState from '../TaskListInitialState.js';
import Note from './Note.js';

class TaskList {

constructor() {
  if (!TaskList._instance) {
    TaskList._instance = this;
    this._taskList = [];
  }

  return TaskList._instance;
  }

  get all() {
    return this._taskList;
  }

  set all(value) {
    this._taskList = value;
  }

  processReminder() {
    this._taskList.map((item,) => {
      let reminderTime = new Date(item.reminderTime);

      if (reminderTime.getTime() <= DateTime.getCurTime().getTime() && item.isNotificationNeeded) {
        alert(`Выполните задачу: ${item.text}`);
        item.isNotificationNeeded = false;
      }
    });
  }

  addTask(task) {
    const newTask = TaskFactory.buildTask(task);

    if (!newTask) {
      return;
    }

    this._taskList.push(newTask);

    if (newTask instanceof Note) {
      new TaskListInitialState('notes').setTask(newTask, 'POST');
    } else {
      new TaskListInitialState('todos').setTask(newTask, 'POST');
    }
  }

  updateTask(id, text, reminderTime, overdueTime) {
    const task = this._taskList.find(item => item.id === id);
    task.text = text;
    task.reminderTime = DateTime.convertDate(reminderTime);
    task.overdueTime = DateTime.convertDate(overdueTime);

    task instanceof Note ? new TaskListInitialState('notes').setTask(task, 'PUT') : new TaskListInitialState('todos').setTask(task, 'PUT');
  }

  processExpiration() {
    this._taskList.map((item) => {
      const expiredTime = new Date(item.overdueTime);
      if (expiredTime.getTime() < (DateTime.getCurTime().getTime()) && !item.isOverdue) {
        item.isOverdue = true;
        new TaskSubscriber().callEvent();
      }
    });
  }

  toggleDoneField(id) {
    this._taskList.forEach((item, index) => {
      if (item.id === id) {
        item.isDone = !item.isDone;
      }
    });
  }

  deleteTask(id) {
    this._taskList.forEach(item => {
      if (item.id === id) {
        this._taskList.splice(id, 1);
        (new TaskListInitialState(`${item.type}s`)).deleteTask(id);
      }
    });
  }

  filterByDone() {
    return this._taskList.filter(item => item.isDone && item);
  }

  filterByUndone() {
    return this._taskList.filter(item => !item.isDone && item instanceof Todo && item);
  }

  filterByExpired() {
    return this._taskList.filter(item => item.isOverdue && item);
  }

  filterByUnexpired() {
    return this._taskList.filter(item => !item.isOverdue && item);
  }

  filterByDate(date) {
    return this._taskList.filter(item => DateTime.isEqualDate(item.overdueTime, date) && item);
  }
}

export default TaskList;
