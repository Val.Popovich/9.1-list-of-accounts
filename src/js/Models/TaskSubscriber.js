class TaskSubscriber {
  subscribe(callback) {
    document.addEventListener('renderAllTaskList', callback);
  }

  callEvent() {
    document.dispatchEvent(new Event('renderAllTaskList'));
  }
}

export default TaskSubscriber;
