class Subscriber {
    subscribe(eventName, callback) {
        document.addEventListener(eventName, callback);
    }

    callEvent(eventName) {
        document.dispatchEvent(new Event(eventName));
    }
}

export default Subscriber;
