import Note from './Note.js';
import Todo from './Todo.js';
import DateTime from './DateTime.js';
import DateFormatter from './DateFormatter.js';
import TaskListInitialState from '../TaskListInitialState.js';

class TaskFactory {

  static buildTask(task) {
    this.params = {
      id: task.id,
      text: task.text,
      creationDate: DateFormatter.formatDateTime(),
      reminderTime: DateTime.convertDate(task.reminderTime),
      overdueTime: DateTime.convertDate(task.overdueTime),
      isDone: task.isDone,
    };

    if (this.params.overdueTime === false || this.params.reminderTime === false) {
      return;
    }

    if (task.type === 'note') {
      return new Note(this.params);
    } else if (task.type === 'todo') {
      return new Todo(this.params);
    }
  }
}

export default TaskFactory;