import Task from './Task.js';

class Todo extends Task {
  constructor(data) {
    super(data);
    this.type = 'todo';
    this.isDone = data.isDone;
  }
}

export default Todo;