import DateFormatter from './DateFormatter.js';

class DateTime {

  static getCurTime() {
    return new Date();
  }

  static convertDate(date){
    if (date === '') {
      return '';
    }

    const regDateAndTime = /^[0-9]{4}-([0][1-9]|[1][0-2]|[0-9])-([0][1-9]|[1-2][0-9]|[3][0-1]) ([0-1][0-9]|[2][0-3]):[0-5][0-9]:[0-5][0-9]$/;

    if (!date.match(regDateAndTime)) {
      alert('Неверный формат даты!');
      return false;
    }

    return DateFormatter.formatDateTime(new Date(date));
  }

  static isOverdueTime(time){
    time = new Date(time);
    return time.getTime() < DateTime.getCurTime().getTime();
  }

  static isEqualDate(filterDate, itemDate){
    return DateFormatter.formatDate(filterDate) === DateFormatter.formatDate(itemDate) ;
  }
}

export default DateTime;