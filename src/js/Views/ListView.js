import TaskList from '../Models/TaskList.js';
import AddTaskHandler from '../Handlers/AddTaskHandler.js';
import Todo from '../Models/Todo.js';
import DeleteTaskHandler from '../Handlers/DeleteTaskHandler.js';
import EditTaskHandler from '../Handlers/EditTaskHandler.js';
import TaskSubscriber from '../Models/TaskSubscriber.js';

class ListView {
  constructor(taskListHTMLElement) {
    this.taskList = new TaskList();
    this.taskListHTMLElement = taskListHTMLElement;
    this.handleInstanceElement();
  }

  renderTaskList(list) {
    this.taskListHTMLElement.innerHTML = '';

    if (!list) {
      return;
    }

    list.forEach((item) => {
      const template = Handlebars.compile(document.querySelector('#instanceTemplate').innerHTML);
      this.taskListHTMLElement.insertAdjacentHTML('beforeend', template({
        'data': item,
        'id': item.id,
        'isTodo': item instanceof Todo,
      }));
    });
  }

  handleInstanceElement() {
    this.taskListHTMLElement.addEventListener('click', (ev) => {
      const listItem = ev.target.closest('.js-task-list__item');

      if (!listItem) {
        return;
      }

      this.handleDeleteBtn(ev.target);
      this.handleEditBtn(ev.target);
      this.handleCheckBox(ev.target);
    });
  }

  handleDeleteBtn(target) {
    const btn = target.closest('.js-item__delete-btn');

    if (!btn) {
      return;
    }

    if (!btn.classList.contains('js-item__delete-btn')){
      return;
    }

    const elemItem = btn.parentNode;
    const elemId = elemItem.dataset.id;

    DeleteTaskHandler.handleDeleteTask(elemId);
  }

  handleEditBtn(target) {
    const btn = target.closest('.js-item__edit-btn');

    if (!btn) {
      return;
    }

    if (!btn.classList.contains('js-item__edit-btn')) {
      return;
    }

    const elemItem = btn.parentNode;
    const elemId = elemItem.dataset.id;

    EditTaskHandler.handleUpdateTask(elemId, btn);
  }

  handleCheckBox(btn) {
    if (!btn.classList.contains('js-item__task-completeness-checkbox')) {
      return;
    }

    const elemId = btn.parentNode.dataset.id;

    this.taskList.toggleDoneField(elemId, btn);

    new TaskSubscriber().callEvent();
  }


  initEventListeners() {
    this.handleForm();
    this.handleExpiredTasksBtn();
    this.handleUnexpiredTasksBtn();
    this.handleAllTasksBtn();
    this.handleDoneTasksBtn();
    this.handleUndoneTasksBtn();
    this.handleSpecifiedDateTasksBtn();
    this.handleRenderTaskList();
  }

  handleRenderTaskList() {
    new TaskSubscriber().subscribe(async () => {
      await this.renderTaskList(this.taskList.all);
    });
  }

  handleForm() {
    const form = document.querySelector('.js-content__toolbar');

    form.addEventListener('click', (ev) => {
      ev.preventDefault();

      if (ev.target.classList.contains('add-btn')) {
        (new AddTaskHandler()).handleAddTask(ev.target.dataset.type);
      }
    });
  }

  handleDoneTasksBtn() {
    const element = document.querySelector('.js-filter-done');

    element.addEventListener('click', async () => {
     await this.renderTaskList(this.taskList.filterByDone());
    });
  }

  handleUndoneTasksBtn() {
    const element = document.querySelector('.js-filter-undone');

    element.addEventListener('click', async () => {
      await this.renderTaskList(this.taskList.filterByUndone());
    });
  }

  handleExpiredTasksBtn() {
    const element = document.querySelector('.js-filter-expired');

    element.addEventListener('click', async () => {
      await this.renderTaskList(this.taskList.filterByExpired());
    });
  }

  handleUnexpiredTasksBtn() {
    const element = document.querySelector('.js-filter-unexpired');

    element.addEventListener('click', async () => {
      await this.renderTaskList(this.taskList.filterByUnexpired());
    });
  }

  handleSpecifiedDateTasksBtn() {
    const element = document.querySelector('.js-filter-date-items');
    const date = document.querySelector('.js-task-data-filter-input');

    element.addEventListener('click', async (event) => {
      event.preventDefault();
      const filteredByDate = this.taskList.filterByDate(date.value);
      await this.renderTaskList(filteredByDate);
    });
  }

  handleAllTasksBtn() {
    const element = document.querySelector('.js-all-items');

    element.addEventListener('click', () => {
      new TaskSubscriber().callEvent();
    });
  }
}

export default ListView;