import TaskList from './Models/TaskList.js';
import ListView from './Views/ListView.js';
import TaskSubscriber from './Models/TaskSubscriber.js';
import TaskListInitialState from './TaskListInitialState.js';
import LocalStorageGetter from './LocalStorageGetter.js';

const taskList = new TaskList();
const listView = new ListView(document.querySelector('.js-content__task-list'));

setInterval(taskList.processExpiration.bind(taskList), 1000);
setInterval(taskList.processReminder.bind(taskList), 1000);

await listView.initEventListeners();

const notesListState = await (new TaskListInitialState('notes')).initTasksState();
localStorage.setItem('notes', JSON.stringify(notesListState));

const todosListState = await (new TaskListInitialState('todos')).initTasksState();
localStorage.setItem('todos', JSON.stringify(todosListState));

taskList.all = LocalStorageGetter.getLocalStorageTasks();

new TaskSubscriber().callEvent();
