import TaskFactory from './Models/TaskFactory.js';

class LocalStorageGetter {

  static getLocalStorageTasks() {
    const notes = [];
    JSON.parse(localStorage.getItem('notes')).forEach(note => {
      notes.push(TaskFactory.buildTask(note));
    });

    const todos = [];
    JSON.parse(localStorage.getItem('todos')).forEach(todo => {
      notes.push(TaskFactory.buildTask(todo));
    })

    return [...notes, ...todos];
  }
}

export default LocalStorageGetter;