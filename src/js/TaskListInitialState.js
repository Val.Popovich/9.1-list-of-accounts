class TaskListInitialState {
  constructor(urlPath) {
    this.host = 'http://localhost:3000/';
    this.url = `${this.host}${urlPath}`;
  }

  async initTasksState() {
    let res = await fetch(`${this.url}`);
    return res.json();
  }

  async setTask(task, method) {
    if (method === 'PUT') {
      this.url = `${this.url}/${task.id}`;
    }

    await fetch(this.url, {
      method: method,
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify(task)
    })
  }

  async deleteTask(id) {
    return await fetch(`${this.url}/${id}`, {
      method: 'DELETE'
    })
  }
}

export default TaskListInitialState;
