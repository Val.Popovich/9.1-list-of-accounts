import TaskList from '../Models/TaskList.js';
import TaskSubscriber from '../Models/TaskSubscriber.js';

class AddTaskHandler {
    handleAddTask(type) {
        const form = document.querySelector('.js-content__toolbar');
        const taskTextInput = form.querySelector('.js-text').value;
        const taskDue = form.querySelector('.js-due-date-and-time').value;
        const taskReminder = form.querySelector('.js-reminder-date-and-time').value;

        if (taskTextInput === '') {
            alert('Задача без текста!')
            return;
        }

        const task = {
          id: uuidv4(),
          text: taskTextInput,
          type: type,
          reminderTime: taskReminder,
          overdueTime: taskDue,
          isDone: false,
        };

        new TaskList().addTask(task);

        new TaskSubscriber().callEvent();
    }
}

export default AddTaskHandler;