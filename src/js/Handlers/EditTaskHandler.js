import TaskList from '../Models/TaskList.js';
import TaskSubscriber from '../Models/TaskSubscriber.js';

class EditTaskHandler {

  static handleUpdateTask(elemIndex, elemBtn) {
    const instanceElem = elemBtn.parentNode;

    const taskTextInput = instanceElem.querySelector('.js-task-text').value;
    const taskReminder = instanceElem.querySelector('.js-task-reminder-date-and-time').value;
    const taskDue = instanceElem.querySelector('.js-task-due-date-and-time').value;

    (new TaskList()).updateTask(elemIndex, taskTextInput, taskReminder, taskDue);

    new TaskSubscriber().callEvent();
  }
}

export default EditTaskHandler;