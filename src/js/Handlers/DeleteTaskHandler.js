import TaskList from '../Models/TaskList.js';
import TaskSubscriber from '../Models/TaskSubscriber.js';

class DeleteTaskHandler {

  static handleDeleteTask(id) {
    new TaskList().deleteTask(id);

    new TaskSubscriber().callEvent();
  }
}

export default DeleteTaskHandler;
